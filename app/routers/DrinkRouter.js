const express = require("express");

const router = express.Router();

router.get("/drinks", (request, response)=> {
    response.status(200).json({
        message: "GET All drinks"
    })
});

router.get('/drinks/:drinkId', (request, response)=> {
    let drinkId = request.params.drinkId;
    response.status(200).json({

        message: "GET drink Id = " + drinkId
    })
});

router.post("/drinks", (request, response)=> {
    response.status(200).json({
        message: "Create drink"
    })
});

router.put('/drinks/:drinkId', (request, response)=> {
    let drinkId = request.params.drinkId;
    response.status(200).json({

        message: "Update drink Id = " + drinkId
    })
});

router.delete('/drinks/:drinkId', (request, response)=> {
    let drinkId = request.params.drinkId;
    response.status(200).json({

        message: "delete drink Id = " + drinkId
    })
});

module.exports = router;